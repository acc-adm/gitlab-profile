# Acc SysAdmin GitLab
GitLab namespace for the system administration of the accelerator control system, managed by BE-CSS-ISA (acc-adm-support@cern.ch).

This README links to other locations of interest to BE-CSS-ISA, and describes the layout for projects in this namespace.

_[Click here to edit this README](https://gitlab.cern.ch/acc-adm/gitlab-profile/-/edit/master/README.md)_

## Links

### Other GitLab Groups and Projects
- https://gitlab.cern.ch/acc-co/docker - Shared container projects
- https://gitlab.cern.ch/kubernetes/ats-poc - ATS-IT kubernetes PoC
- https://gitlab.cern.ch/be-cem-edl/fec/operating-system/fec-os-distribution - Front-end computer operating system
- https://gitlab.cern.ch/industrial-controls/projs/science-gateway-education-labs/sgel-nginx - SGEL proxy
- https://gitlab-dev.cern.ch/groups/acc-adm - GitLab dev instance namespace 

#### Old namespaces to be migrated here
- https://gitlab.cern.ch/acc - Old namespace (still active)
- https://gitlab.cern.ch/lumens - Old LUMENS namespace
- https://gitlab.cern.ch/cosmos - Old COSMOS namespace
- https://gitlab.cern.ch/acc_ansible_c8 - Old cs8 ansible


### Wikis
- **https://confluence.cern.ch/display/ACCADM**
- https://confluence.cern.ch/display/ACCVIDEO - VIDEO
- https://confluence.cern.ch/display/LUM - LUMENS
- https://confluence.cern.ch/display/COS - COSMOS
- https://acc-containers.docs.cern.ch - Containers
- https://confluence.cern.ch/display/CONT - Containers (old)

### Issues
- **https://issues.cern.ch/projects/ACCADM**
- https://issues.cern.ch/projects/VIDEO - Video
- https://issues.cern.ch/projects/LUM - LUMENS
- https://issues.cern.ch/projects/COS - COSMOS
- https://issues.cern.ch/projects/ROG - Remote Operations Gateway


## GitLab Projects Layout
The projects are categorized in [cloud service model layers ](https://www.ibm.com/topics/iaas-paas-saas#What+are+Iaas,+Paas+and+Saas?), taking inspiration from [Service-oriented architecture (SOA)](https://en.wikipedia.org/wiki/Service-oriented_architecture). Although we don't follow neccesarilly follow the complete SOA architectural style, it still provides as great structural template.
These cloud service models are often referenced visually, like:

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Flh3.googleusercontent.com%2F2RHSrFzEE08zN6GWTt584SEN81YKqg3uHvdEezfY1YtPfLGreHQAkvYG0xEIkTmQXti_u8Ku0j-3pFRqomNuwqVuAT2T3MGyK1Jn_Ob4iQo4PXjoWlyMO36_rX_sIt12QVLvgxhg&f=1&nofb=1&ipt=6b227772ea76b3ca9c57bd5764d5218607ee17326b843b936920824b584a70da&ipo=images){width=400}


But only categorizing vertically like in the image above does not provide a complete layout, as some project will be horizontal, spanning across all vertical layers, like [Monitoring](https://gitlab.cern.ch/acc-adm/monitor), [Security](https://gitlab.cern.ch/acc-adm/security), [DevOps](https://gitlab.cern.ch/acc-adm/devops), etc...

This results in a mix of vertically and horizontally sliced categories we spread our projects over.

The [Infrastrucute group](https://gitlab.cern.ch/acc-adm/infra) is used as generic category for projects that don't easily fit in more specific groups.
